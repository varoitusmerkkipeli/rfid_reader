#include <EEPROM.h>
#include <MIDI.h>

#include <SoftwareSerial.h>

/*
 SM130 based RFID reader
 
 Copyright © 2014 Jari Suominen <jari@tasankokaiku.com>
 This work is free. You can redistribute it and/or modify it under the
 terms of the Do What The Fuck You Want To Public License, Version 2,
 as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.
 
 
 Based on hardware v13:
 D7 -> SM130 RX
 D8 -> SM130 TX
 
 Note: SM130 Reset attached to D13 (aka status LED)
 
 Detects if predefined tags are in range.
 When tags enter or exit the range, midi noteon and noteoff
 messages are sent.
 
 */

#define MAX_ATTEMPTS 50
#define MAX_TAGS 16

boolean DEBUG = false;

SoftwareSerial sm130(6, 7);

//Global var
int message[50];
boolean antennaOnPending = false; // Hack as SM130 seems to send antenna on -message back twice...
int tagsFound = 0;

/* Addresses of known tags. */
long tags[MAX_TAGS];
boolean note_ons[MAX_TAGS];
boolean detectedOnLatestIteration[MAX_TAGS];
boolean allocateNewTag = false;
int tagToBeAllocated = 0;

long antennaSwitchedOnMS = millis();

void setup() {
  delay(1000);  
  if (DEBUG) {
    Serial.begin(115200);
    Serial.println("RFID GO");
  } 
  else {
    MIDI.begin(MIDI_CHANNEL_OMNI);
    MIDI.turnThruOff();
    MIDI.setHandleNoteOn(HandleNoteOn);
  }

  sm130.begin(19200);
  sm130.listen();
  delay(100);

  setAntenna(true);
  delay(500);

  loadTagArray();
  digitalWrite(13,LOW);
}

boolean nextTagWillBeHalted = false;

void loop() {
  updateSM130();

  if (!DEBUG) {
    MIDI.read();  
  } 
  else {
    if (Serial.available()) {
      Serial.read();
      nextTagWillBeHalted = true;  
      //firmware_version();
    }
  }   
}

/*
  Is used for entering new tags to the system.
  Will associate tag that is read next to a id number given as note.
  If sent higher note than system has slots for tags, will switch back
  to normal operational mode.
 */
void HandleNoteOn (byte channel, byte note, byte velocity) {
  if (note < MAX_TAGS) {
    allocateNewTag = true;
    tagToBeAllocated = note; 
    digitalWrite(13,HIGH);
    MIDI.sendNoteOn(tagToBeAllocated,1,1);
  } else {
    allocateNewTag = false;
    MIDI.sendNoteOn(127,127,1);
  }  
}

int responseIterator = 0;
void updateSM130() {
  while(sm130.available()){
    message[responseIterator] = sm130.read();
    if (responseIterator > 0 && message[responseIterator]==0 && message[responseIterator-1]==255) {
      responseIterator = 0;
    } 
    else if (responseIterator == message[0]+1) {
      processMessage();
      responseIterator++;
    } 
    else {
      responseIterator++;  
    }
  }
}

int attempts = 0;
void processMessage() {
  // calculating checksum
  int checksum = 0;
  for (int i = 0; i < message[0]+1; i++) {
    checksum += message[i];
  }

  if (!((uint8_t)checksum == message[message[0]+1])) {
    /* Message was corrupted */
    return;
  }

  switch (message[1]) {
  case 0x80: // reset
    Serial.print(millis());
    Serial.print(" FIRMWARE VERSION");
    //rescan();
    break;
  case 0x81: // reset
    Serial.print(millis());
    Serial.print(" FIRMWARE VERSION");
    //rescan();
    break;
  case 0x82: // Seek tag
  case 0x83: // Select tag
    if (message[0]==2) {
      if (message[2]==0x4c) {
        //Serial.println("Seek tag: Command in progress");
      }
      else if (message[2]==0x4e) {
        //if (DEBUG) Serial.print(millis());
        //if (DEBUG) Serial.println(" Seek tag: nothing found!");
        //for (int i = 0; i < MAX_TAGS; i++)
        //  detectedOnLatestIteration[i] = false;   
        //select();
        rescan();
        sendNoteOffs(); 
        return;
        //delay(10);
        attempts++;
        if (attempts < MAX_ATTEMPTS) {
          select();
          return; 
        }
        //        /* Select tag: no tags found */
        //        if (tagsFound==0) {
        //          /* No tags within range */
        //          select();
        //        } 
        if (millis() - antennaSwitchedOnMS > 4000) {
          /* All tags within range read */
          //reset();
          //Serial.print("kjdfs");
          sendNoteOffs(); 
          setAntenna(false);
        } 
        else {
          select();  
        }

      }
      else if (message[2]==0x55) {
        if (DEBUG) Serial.println("Seek/Select tag: Command in progress ... but RF field is off");
        select();
      }
      else {
        if (DEBUG) Serial.print("Seek tag: FATAL ERROR!");
      }
    } 
    else {
      switch (message[2]) {
      case 1:
        //Serial.print("Seek tag: Mifare Ultralight detected.");
        break;
      case 2: 
        {   
          if (DEBUG) Serial.print(millis());
          if (DEBUG) Serial.print(" Seek tag: Mifare Standard 1K detected: ");
          unsigned long a = 0;
          for (int i = 0; i < 4; i++) {
            a = a << 8;
            a += message[i+3];
            //Serial.print(message[i+3],HEX);
            //Serial.print(" ");
          }
          if (DEBUG) Serial.print(a);

          if (allocateNewTag) {
            tags[tagToBeAllocated] = a;
            for (int i = 0; i < 4; i++) {
              EEPROM.write(tagToBeAllocated*4 + i,message[i+3]);
            }  
            allocateNewTag = false;
            MIDI.sendNoteOn(tagToBeAllocated,127,1);
            digitalWrite(13,LOW); 
          } 
          else {
            midiUpdate(a);
            sendNoteOffs();
            tagsFound++;
          }
          if (nextTagWillBeHalted) {
            haltTag();
          } 
          else {
            select();
          }
          break;
        }
      case 3:
        //Serial.print("Select tag: Mifare Classic 4K detected.");
        break;
      default:
        Serial.print("Select tag: Unknown tag type ");
        Serial.print(message[2]);
        Serial.println(" detected.");
        break;
      }
    }
    break;
  case 0x90: // Set antenna power
    if (message[2]==0x00) { // was turned off
      if (DEBUG) Serial.println("Antenna was turned off.");
      delay(5000);
      setAntenna(true);
    } 
    else {
      if (antennaOnPending) {
        //Serial.println("switching on ... please wait.");
        antennaOnPending = false;
      } 
      else {
        if (DEBUG) Serial.println("Antenna switched on."); 
        rescan();
        //select(); 
      }
    } 
    break;
  case 0x93: // halt tag
    nextTagWillBeHalted = false;
    select();
    break;
  default:
    break;

  }
}


void setAntenna(boolean on) {
  sm130.write((uint8_t)255);
  sm130.write((uint8_t)0x00);
  sm130.write((uint8_t)0x02);
  sm130.write((uint8_t)0x90);
  if (on) {
    sm130.write((uint8_t)0x01);
    sm130.write((uint8_t)0x93);
  } 
  else {
    sm130.write((uint8_t)0x00);
    sm130.write((uint8_t)0x92);
  }
}

void haltTag()
{
  sm130.write((uint8_t)255);
  sm130.write((uint8_t)0);
  sm130.write((uint8_t)1);
  sm130.write((uint8_t)147);
  sm130.write((uint8_t)148);
}

void seek_tag()
{
  sm130.write((uint8_t)255);
  sm130.write((uint8_t)0);
  sm130.write((uint8_t)1);
  sm130.write((uint8_t)0x82);
  sm130.write((uint8_t)131);
}

void firmware_version()
{
  sm130.write((uint8_t)255);
  sm130.write((uint8_t)0);
  sm130.write((uint8_t)1);
  sm130.write((uint8_t)0x81);
  sm130.write((uint8_t)0x82);
}

void select()
{
  sm130.write((uint8_t)255);
  sm130.write((uint8_t)0);
  sm130.write((uint8_t)1);
  sm130.write((uint8_t)131);
  sm130.write((uint8_t)132);
}

void reset()
{
  sm130.write((uint8_t)255);
  sm130.write((uint8_t)0);
  sm130.write((uint8_t)1);
  sm130.write((uint8_t)0x80);
  sm130.write((uint8_t)0x81);
}

void rescan() {
  for (int i = 0; i < MAX_TAGS; i++) {
    detectedOnLatestIteration[i] = false;
  }
  tagsFound = 0;
  attempts = 0;
  antennaSwitchedOnMS = millis();
  select();
}

/*
Well send noteon message if tag was not close to reader
 during previous scan iteration.
 */
void midiUpdate(long address) {
  for (int i = 0; i < MAX_TAGS; i++) {
    if (address == tags[i]) {
      detectedOnLatestIteration[i] = true;
      if (!note_ons[i]) {
        if (!DEBUG) {
          MIDI.sendNoteOn(i,127,1); 
        }
        note_ons[i] = true;
      } 
      return;
    } 
    else {

    }
  }  
}

/*
All tags that were not detected are not close to reader.
 If one of them was there before last scan, noteoff message
 will be sent.
 */
void sendNoteOffs() {
  for (int i = 0; i < MAX_TAGS; i++) {
    if (DEBUG) {
      if (detectedOnLatestIteration[i]) {
        Serial.print(i);
      } 
      else {
        Serial.print(".");  
      }
    }
    if (!detectedOnLatestIteration[i] && note_ons[i]) {
      if (!DEBUG) {
        MIDI.sendNoteOff(i,0,1);
      }
      note_ons[i] = false; 
    } 
  }
  if (DEBUG) {
    Serial.println("");  
  }
}

void loadTagArray() {
  for (int i = 0; i < MAX_TAGS; i++) {
    tags[i] = 0;
    for (int a = 0; a < 4; a++) {
      tags[i] = tags[i] << 8;
      tags[i] += EEPROM.read(i*4 + a);
    }
    note_ons[i] = false;
    detectedOnLatestIteration[i] = false;
    if (DEBUG) {
      Serial.print(i);
      Serial.print(" ");
      Serial.println(tags[i]);
    }
  }  
}
